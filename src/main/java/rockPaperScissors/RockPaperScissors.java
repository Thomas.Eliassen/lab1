package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
    	
    	boolean yesNoFlag = true;
    	String start = "";
    	String rePrompt = "";
    	String[] arrayChoices = rpsChoices.toArray(new String[rpsChoices.size()]);
    	String result = "";
    	String yesNo = "";
    	String computerChoice = "";
    	String humanChoice = "";
    	String score = "";
    	String roundResult = "";
    	
    	while (yesNoFlag == true) {
    		double randomDouble = Math.random()*(3);
    		int randomInt = (int) Math.floor(randomDouble);
    		computerChoice = arrayChoices[randomInt];
    		start = String.format("Let's play round %d", roundCounter);
     		System.out.println(start);
    	
     		humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
    		while ((humanChoice.equalsIgnoreCase("scissors") || humanChoice.equalsIgnoreCase("rock") || humanChoice.equalsIgnoreCase("paper")) == false) {
    			rePrompt = String.format("I do not understand %s. Could you try again?", humanChoice);
    			humanChoice = readInput(rePrompt);
    		}
    		if (humanChoice.equalsIgnoreCase(computerChoice)) {
    			result = "Its a tie!";
    		}
    		
    		if (humanChoice.equalsIgnoreCase("scissors")) {
    			if (computerChoice.equals("paper")) {
    				result = "Human wins!";
    				humanScore++;
    			}
    			if (computerChoice.equals("rock")) {
    				result = "Computer wins!";
    				computerScore++;
    			}
    		}
    		if (humanChoice.equalsIgnoreCase("paper")) {
    			if (computerChoice.equals("rock")) {
    				result = "Human wins!";
    				humanScore++;
    			}
    			if (computerChoice.equals("scissors")) {
    				result = "Computer wins!";
    				computerScore++;
    			}
    		}
    		if (humanChoice.equalsIgnoreCase("rock")) {
        		if (computerChoice.equals("scissors")) {
       				result = "Human wins!";
       				humanScore++;
        		}
       			if (computerChoice.equals("paper")) {
       				result = "Computer wins!";
       				computerScore++;
       			}
       		}
    			
    		roundResult = String.format("Human chose %s computer chose %s. %s", humanChoice, computerChoice, result);
    		System.out.println(roundResult);
    		
    		score = String.format("Score: human %d, computer %d",humanScore, computerScore);
    		System.out.println(score);
    		
    		roundCounter++;
    		yesNo = "";
    		do {
    			yesNo = readInput("Do you wish to continue playing? (y/n)?");
   			}while ((yesNo.equalsIgnoreCase("y")|| yesNo.equalsIgnoreCase("n")) == false);
   			if (yesNo.equalsIgnoreCase("n")) {
   				yesNoFlag = false;
   				System.out.println("Bye bye :)");
    		}
    	}

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
